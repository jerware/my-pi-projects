#!/usr/bin/python

from time import sleep
from LPD8806 import *

num = 260;
led = LEDStrip(num)
led.setChannelOrder(ChannelOrder.BRG)
led.all_off()

LEDStrip.setRGB(1, 1, 100, 0, 0)
led.update()
